/**
 * @date	2013-04-29
 *
 * ProjectEuler.net 
 * Multiples of 3 and 5
 * Problem 1
 * 
 * Question:
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
 * Find the sum of all the multiples of 3 or 5 below 1000.
 *
 * Solution:
 * 	Arithmetic progressions:
 *		sum = 3 + 6 + ... + 996 + 999
 *	
 *	   	    = (a + b) * (n / 2)
 *	
 *		a = first term
 *		b = last term
 *		n = number of terms
 *	
 *		n = b / first term
 *
 *      solution = SumDivisibleBy(3)+SumDivisibleBy(5)-SumDivisibleBy(15)
 *
 */

class problem1 {
	final static int target = 999;

	static int sumDivisibleBy(int n) {
		int p = target / n;
		return n * (p * (p + 1)) / 2;
	}

	public static void main(String[] args) {
		System.out.println("Output = " + (
			sumDivisibleBy(3) +
			sumDivisibleBy(5) -
			sumDivisibleBy(15)
		));
	}
}
